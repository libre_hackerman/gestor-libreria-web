/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package comun;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

/**
 * Clase-librería que almacena los mensajes de error de las diferentes excepciones
 * @author Esteban
 */
public class Errores {
    public static String getError(Exception e) {
        String mensajito;
        
        if (e instanceof SQLException) {
            mensajito = "Error en la base de datos";
        } else if (e instanceof InstantiationException) {
            mensajito = "Error de instanciación";
        } else if (e instanceof IllegalAccessException) {
            mensajito = "Acceso ilegal";
        } else if (e instanceof IllegalArgumentException){
            mensajito = "Argumento ilegal";
        } else if (e instanceof InvocationTargetException) {
            mensajito = "Error de invocación";
        } else if (e instanceof ClassNotFoundException) {
            mensajito = "Clase no encontrada";
        } else if (e instanceof IOException) {
            mensajito = "Error de I/O";
        } else {
            mensajito = e.getMessage();
        }
     
        return mensajito;
    }
}
