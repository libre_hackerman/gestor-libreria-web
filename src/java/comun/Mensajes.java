/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package comun;

/**
 * Clase-librería que almacena diferentes mensajes informativos. Los mensajes
 * invariables se almacenan en constantes, mientras que los que tengan un sujeto
 * variable (libro, usuario, etc) son generados mediante métodos
 * @author Esteban
 */
public class Mensajes {
    public static final String MSG_NO_CAMBIOS = "No se ha realizado ningún cambio";
    public static final String MSG_NO_LOGIN = "Usuario y/o contraseña incorrectos";
    public static final String MSG_PASS_DIFF = "Las contraseñas no coinciden";
    
    public static String msgInsertado(String loQue) {
        return loQue + " insertado";
    }
    
    public static String msgBorrado(String loQue) {
        return loQue + " borrado";
    }
    
    public static String msgEditado(String loQue) {
        return loQue + " editado";
    }
    
    public static String msgYaExiste(String loQue) {
        return loQue + " ya existe, no se va a insertar";
    }
    
    public static String msgNoEncontrado(String loQue) {
        return loQue + " no encontrado";
    }
}
