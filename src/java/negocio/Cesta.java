/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package negocio;

import java.util.ArrayList;
import java.util.List;

public class Cesta {
    private final ArrayList<LineaPedido> cesta = new ArrayList<>();
    
    public void sumarLibro(Libro libro) {
        // Busca el libro en la cesta
        boolean encontrado = false;
        for (int i = 0; i < cesta.size() && !encontrado; i++) {
            if (cesta.get(i).getLibro().getTitulo().equals(libro.getTitulo())) {
                cesta.get(i).incrementar();
                encontrado = true;
            }
        }
        
        // Si no existía ya en la cesta, lo añade
        if (!encontrado)
            cesta.add(new LineaPedido(libro, 1));
    }
    
    public void restarLibro(Libro libro) {
        // Busca el libro en la cesta
        boolean encontrado = false;
        for (int i = 0; i < cesta.size() && !encontrado; i++) {
            if (cesta.get(i).getLibro().getTitulo().equals(libro.getTitulo())) {
                cesta.get(i).decrementar();
                
                // Si la cantidad queda en 0, lo elimina de la cesta
                if (cesta.get(i).getCantidad() == 0)
                    cesta.remove(i);
                
                encontrado = true;
            }
        }
    }
    
    public int numArticulos() {
        int acm = 0;
        
        for (LineaPedido l : cesta)
            acm += l.getCantidad();
        
        return acm;
    }
    
    public int getTotal() {
        int acm = 0;
        
        for (LineaPedido l : cesta)
            acm += l.getTotal();
        
        return acm;
    }
    
    public List<LineaPedido> getLineas() {
        return cesta;
    }
    
    public void vaciar() {
        cesta.clear();
    }
}
