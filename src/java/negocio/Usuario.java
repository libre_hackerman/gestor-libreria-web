/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package negocio;

import persistencia.MetaDTO;

@MetaDTO(nombreTabla = "usuarios", campoUnique = "nombre")
public class Usuario {
    private String nombre;
    private String passHash;
    private int id, nivel;

    public Usuario(int id, String nombre, String passHash, int nivel) {
        this.id = id;
        this.nombre = nombre;
        this.passHash = passHash;
        this.nivel = nivel;
    }

    public Usuario(String nombre) {
        this.id = -1;
        this.nombre = nombre;
        this.passHash = "";
        this.nivel = 0;
    }
    
    public Usuario() {
        this.id = -1;
        this.nombre = "";
        this.passHash = "";
        this.nivel = 0;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPassHash(String passHash) {
        this.passHash = passHash;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPassHash() {
        return passHash;
    }

    public int getNivel() {
        return nivel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
