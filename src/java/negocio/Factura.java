/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package negocio;

import persistencia.MetaDTO;

@MetaDTO(nombreTabla = "facturas")
public class Factura {
    private int id;
    private long fecha;
    private int idUsuario;

    public Factura(int id, long fecha, int idUsuario) {
        this.id = id;
        this.fecha = fecha;
        this.idUsuario = idUsuario;
    }

    public int getId() {
        return id;
    }

    public long getFecha() {
        return fecha;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFecha(long fecha) {
        this.fecha = fecha;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    
    
}
