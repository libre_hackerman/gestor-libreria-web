/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package negocio;

import persistencia.MetaDTO;

@MetaDTO(nombreTabla = "libros")
public class Libro {

    private String titulo, autor, tema;
    private int id, paginas, precio;
    private boolean cartone, rustica, tapaDura, novedad;

    /**
     * Constructor parametrizado completo
     * @param id
     * @param titulo
     * @param autor
     * @param tema
     * @param paginas
     * @param precio
     * @param cartone
     * @param rustica
     * @param tapaDura
     * @param novedad 
     */
    public Libro(int id, String titulo, String autor, String tema, int paginas, int precio, boolean cartone, boolean rustica, boolean tapaDura, boolean novedad) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
        this.precio = precio;
        this.tema = tema;
        this.cartone = cartone;
        this.rustica = rustica;
        this.tapaDura = tapaDura;
        this.novedad = novedad;
    }
    
    /**
     * Constructor parametrizado con solo el título
     * <i>Los demás atritubutos son inicializados con valores por defecto</i>
     * @param titulo 
     */
    public Libro(int id) {
        this.id = id;
        titulo = "";
        autor = "";
        tema = "";
        paginas = -1;
        precio = 0;
        cartone = false;
        rustica = false;
        tapaDura = false;
        novedad = true;
    }
    
    /**
     * Constructor por defecto
     */
    public Libro() {
        titulo = "";
        autor = "";
        tema = "";
        paginas = -1;
        precio = 0;
        cartone = false;
        rustica = false;
        tapaDura = false;
        novedad = true;
    }
    
    // --- Getters  y setters---
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    public boolean isCartone() {
        return cartone;
    }

    public void setCartone(boolean cartone) {
        this.cartone = cartone;
    }

    public boolean isRustica() {
        return rustica;
    }

    public void setRustica(boolean rustica) {
        this.rustica = rustica;
    }

    public boolean isTapaDura() {
        return tapaDura;
    }

    public void setTapaDura(boolean tapaDura) {
        this.tapaDura = tapaDura;
    }

    public boolean isNovedad() {
        return novedad;
    }

    public void setNovedad(boolean novedad) {
        this.novedad = novedad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
