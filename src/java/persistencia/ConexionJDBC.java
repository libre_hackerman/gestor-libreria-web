/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package persistencia;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * Clase que gestiona las conexiones al pool
 * @author Esteban
 */
public class ConexionJDBC {
    
    private final DataSource dataSource;
    
    /**
     * Constructor que carga el pool conexiones de META-INF/context.xml
     * @throws NamingException
     */
    public ConexionJDBC() throws NamingException {
        Context initCtx = new InitialContext();
        Context envCtx = (Context) initCtx.lookup("java:comp/env");
        dataSource = (DataSource) envCtx.lookup("jdbc/PoolDB");
    }
    
    /**
     * Crea una nueva Connection a la base de datos en modo transaccional
     * @return Una Connection nueva
     * @throws SQLException Si la base de datos da error
     */
    public Connection nuevaConnection() throws SQLException {
        Connection conexion = dataSource.getConnection();
        conexion.setAutoCommit(false);  // Modo transaccional
        return conexion;
    }
}
