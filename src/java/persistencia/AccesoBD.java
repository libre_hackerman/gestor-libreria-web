/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package persistencia;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.ArrayList;
import java.util.Map.Entry;
import javax.naming.NamingException;

/**
 * Esta clase provee a los DAO de métodos para interactuar con la base de datos
 * @author Esteban
 */
public abstract class AccesoBD {
    public static final int LIMITE_STRINGS = 30;  // Tamaño máximo de los campos de texto
    public static final int COD_NO_CAMBIOS = -1;  // DEBE ser un núm negativo
    private final ConexionJDBC conJDBC;
    
    /**
     * Constructor de la clase. Crea el gestor de conexiones
     * @throws NamingException
     */
    public AccesoBD() throws NamingException {
        conJDBC = new ConexionJDBC();
    }
    
    /**
     * Ejecuta una orden SQL de inserción
     * @param sql Orden SQL
     * @param commit true hace commit en la transacción, false lo omite
     * @return clave primaria (id) del registro insertado, o COD_NO_CAMBIOS si no se inserta
     * @throws SQLException si ocurre un error al ejecutar la orden
     */
    protected int ejecutarInsert(String sql, boolean commit) throws SQLException {
        int id = COD_NO_CAMBIOS;
        
        try (Connection conexion = conJDBC.nuevaConnection()) {
            try {
                PreparedStatement consulta = conexion.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                if (consulta.executeUpdate() > 0) {
                    if (commit)
                        conexion.commit();
                    ResultSet rs = consulta.getGeneratedKeys();
                    rs.next();
                    id = rs.getInt("id");
                } else {
                    conexion.rollback();
                }

            } catch (SQLException e) {
                conexion.rollback();
                throw e;
            }
        }
        
        return id;
    }
    
    /**
     * Ejecuta una orden SQL de actualización (UPDATE, DELETE...)
     * @param sql Orden SQL
     * @return true si ha modificado al menos un registro
     * @throws SQLException si ocurre un error al ejecutar la orden
     */
    protected boolean ejecutarUpdate(String sql) throws SQLException {
        boolean haModificado = false;
        
        try (Connection conexion = conJDBC.nuevaConnection()) {
            try {
                PreparedStatement consulta = conexion.prepareStatement(sql);
                haModificado = consulta.executeUpdate() > 0;
                if (haModificado) {
                    conexion.commit();
                } else {
                    conexion.rollback();
                }

            } catch (SQLException e) {
                conexion.rollback();
                throw e;
            }
        }
        
        return haModificado;
    }
    
    /**
     * Ejecuta una consulta con el fin de obtener un arraylist con los registros
     * seleccionados. <b>Es necesario que la consulta pida todos los campos que 
     * tengan un método setter en su DTO</b>
     * @param sql Query SQL
     * @param clase clase del DTO
     * @return Arraylist de Objects casteables a <i>clase</i>
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    protected ArrayList<Object> ejecutarConsulta(String sql, Class clase) 
            throws SQLException, InstantiationException, IllegalAccessException, 
            IllegalArgumentException, InvocationTargetException {
        ArrayList<Object> objetos = new ArrayList<>();
        try (Connection conexion = conJDBC.nuevaConnection()) {
            PreparedStatement consulta = conexion.prepareStatement(sql);
            ResultSet rs = consulta.executeQuery();
            while (rs.next()) {
                Object objeto = clase.newInstance();

                for (Entry<String, Method> setter : Comunes.obtenerMetodos(clase, "set").entrySet()) {
                    setter.getValue().invoke(objeto, rs.getObject(setter.getKey()));
                }

                objetos.add(objeto);
            }
        }
        return objetos;
    }
    
}
