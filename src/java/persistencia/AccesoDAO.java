/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.naming.NamingException;

/**
 * DAO genérico. Contiene las operaciones de inserción, borrado, modificación 
 * y obtención de la persistencia
 * @author Esteban
 */
public class AccesoDAO extends AccesoBD {
    
    public static final int COD_MODIFICADO = 1;
    public static final int COD_LARGO = 3;
    
    private final UtilSql utilSql;
    
    /**
     * Constructor por defecto que recupera la instancia del gestor de SQLs
     * @throws IOException error al cargar fichero de querys
     * @throws NamingException
     */
    public AccesoDAO() throws NamingException, IOException {
        super();
        utilSql = UtilSql.getInstance();
    }
    
    /**
     * Inserta un objeto nuevo en la persistencia
     * @param objeto objeto a insertar
     * @param commit true hace commit en la transacción, false lo omite
     * @return id del registro insertado o COD_NO_CAMBIOS si no se inserta
     * @throws SQLException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    public int insertar(Object objeto, boolean commit) throws SQLException, InstantiationException, IOException, 
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return ejecutarInsert(utilSql.sqlInsertar(objeto), commit);
    }
    
    /**
     * Inserta un array de objetos en la persistencia
     * @param objetos objetos a insertar
     * @return array de las claves primarias de los registros insertados o null si hubo un error
     * @throws SQLException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    public int[] insertar(Object[] objetos) throws SQLException, InstantiationException, IOException, 
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        int[] claves = new int[objetos.length];
        
        boolean correcto = true;
        for (int i = 0; i < objetos.length && correcto; i++) {
            claves[i] = ejecutarInsert(utilSql.sqlInsertar(objetos[i]), i == objetos.length-1);
            
            correcto = claves[i] != COD_NO_CAMBIOS;
        }
        
        return correcto ? claves : null;
    }

    /**
     * Borra un objeto de la persistencia
     * @param objeto a borrar
     * @return true si la operación tuvo éxito
     * @throws SQLException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IOException 
     */
    public boolean borrar(Object objeto) throws SQLException, IllegalAccessException, 
            IllegalArgumentException, InvocationTargetException, IOException {
        return ejecutarUpdate(utilSql.sqlBorrar(objeto));
    }

    /**
     * Busca un registro en la persistencia en base a un objeto con al
     * al menos su clave primaria o unique, devolviendo el objeto con los campos completos
     * @param objeto objeto con al menos su clave primaria o unique
     * @return objeto con los campos completos
     * @throws SQLException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     * @throws java.lang.NoSuchMethodException 
     */
    public Object obtener(Object objeto) throws SQLException, InstantiationException, IOException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        boolean buscarId = true;
        
        // Comprueba si el DTO tiene una clave unique definida
        if (!Comunes.obtenerUnique(objeto.getClass()).equals("")) {
            Method getId = objeto.getClass().getMethod("getId");
            
            // Busca por id (clave primaria) si hay un id establecido en el objeto 
            // (si no lo hay, encontrará un negativo)
            buscarId = ((int) getId.invoke(objeto)) >= 0;
        }
        
        ArrayList<Object> objetos = ejecutarConsulta(buscarId ? utilSql.sqlBuscar(objeto) : 
                utilSql.sqlBuscarUnique(objeto), objeto.getClass());
        
        if (!objetos.isEmpty())
            return objetos.get(0);
        else
            return null;
    }
    
    /**
     * Cambia los datos de un objeto ya existente en la persistencia
     * @param modificado objeto con los datos nuevos
     * @return Código de salida de la operación
     * @throws SQLException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    public int modificar(Object modificado) throws SQLException, IOException,
            IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        return ejecutarUpdate(utilSql.sqlModificar(modificado)) ? COD_MODIFICADO : COD_NO_CAMBIOS;
    }

    /**
     * Obtiene todos los objetos de un DTO concreto almacenados en la persistencia
     * @param nombreClase cadena paquete.clase del DTO
     * @return ArrayList con todos los objetos
     * @throws SQLException
     * @throws InstantiationException
     * @throws IOException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     * @throws java.lang.ClassNotFoundException 
     */
    public ArrayList<Object> obtener(String nombreClase) throws SQLException, InstantiationException, IOException, 
            IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {
        
        // AL<Object> ==UpCast a Object==> Object ==DownCast a AL<Object>==> AL<Object>
        return (ArrayList<Object>) ((Object) ejecutarConsulta(utilSql.sqlObtener(Class.forName(nombreClase)), Class.forName(nombreClase)));
    }
}
