/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map.Entry;
import java.util.Properties;

/**
 * Clase que gestiona y crea querys SQL.
 * 
 * <i>Esta clase es singleton con la finalidad de que todos los DAOs que
 * la usen compartan la misma instancia y por tanto el mismo listado de
 * querys generadas</i>
 * @author Esteban
 */
public class UtilSql {
    private static final File FICHERO = new File("querys.properties");
    private final Properties ficheroQuerys = new Properties();
    private static UtilSql instancia;
    
    /**
     * Constructor privado que carga el fichero almacén de SQLs
     * @throws IOException Error I/O al cargar las querys
     */
    private UtilSql() throws IOException {
        if (FICHERO.exists())
            ficheroQuerys.load(new FileInputStream(FICHERO));
    }
    
    /**
     * Devuelve la instancia de UtilSql
     * @return instancia de UtilSql
     * @throws IOException Error I/O al cargar las querys
     */
    public static UtilSql getInstance() throws IOException {
        if (instancia == null) {
            instancia = new UtilSql();
        }
        return instancia;
    }
    
    // --------- Interfaz pública -----------
    
    /**
     * Crea la query para insertar la instancia de un DTO
     * @param ob una instancia de un DTO
     * @return la query generada
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     * @throws IOException Error I/O al escribir las querys
     */
    public String sqlInsertar(Object ob) throws IllegalAccessException, IllegalArgumentException, 
            InvocationTargetException, IOException {
        String clave = "insertar-" + ob.getClass().getSimpleName();
        String sql = ficheroQuerys.getProperty(clave);
        
        if (sql == null) {
            sql = crearInsertar(ob.getClass());
            ficheroQuerys.setProperty(clave, sql);
            ficheroQuerys.store(new FileOutputStream(FICHERO), "");
        }
        
        sql = parametrizarTodos(ob, sql);
        
        return sql;
    }
    
    /**
     * Crea la query para hacer un borrado físico de la instancia de un DTO en la base de datos
     * @param ob una instancia de un DTO
     * @return la query generada
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     * @throws IOException Error I/O al escribir las querys
     */
    public String sqlBorrar(Object ob) throws IllegalAccessException, IllegalArgumentException, 
            InvocationTargetException, IOException {
        String clave = "borrar-" + ob.getClass().getSimpleName();
        String sql = ficheroQuerys.getProperty(clave);
        
        if (sql == null) {
            sql = crearBorrar(ob.getClass());
            ficheroQuerys.setProperty(clave, sql);
            ficheroQuerys.store(new FileOutputStream(FICHERO), "");
        }
        
        sql = parametrizarClave(ob, sql, false);
        
        return sql;
    }
    
    /**
     * Crea la query para buscar un registro concreto según su clave primaria
     * @param ob la instancia de un DTO con al menos su clave primaria
     * @return la query generada
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IOException Error I/O al escribir las querys
     */
    public String sqlBuscar(Object ob) throws IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, IOException {
        String clave = "buscar-" + ob.getClass().getSimpleName();
        String sql = ficheroQuerys.getProperty(clave);
        
        if (sql == null) {
            sql = crearBuscar(ob.getClass());
            ficheroQuerys.setProperty(clave, sql);
            ficheroQuerys.store(new FileOutputStream(FICHERO), "");
        }
        
        sql = parametrizarClave(ob, sql, false);
        
        return sql;
    }
    
    /**
     * Crea la query para buscar un registro concreto según su campo unique
     * @param ob la instancia de un DTO con al menos su campo unique
     * @return la query generada
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IOException Error I/O al escribir las querys
     */
    public String sqlBuscarUnique(Object ob) throws IllegalAccessException, IllegalArgumentException,
            InvocationTargetException, IOException {
        String clave = "buscarUnique-" + ob.getClass().getSimpleName();
        String sql = ficheroQuerys.getProperty(clave);
        
        if (sql == null) {
            sql = crearBuscarUnique(ob.getClass());
            ficheroQuerys.setProperty(clave, sql);
            ficheroQuerys.store(new FileOutputStream(FICHERO), "");
        }
        
        sql = parametrizarClave(ob, sql, true);
        
        return sql;
    }
    
    /**
     * Crea la query para cambiar los datos de un registro
     * @param modificado DTO del registro con la información nueva
     * @return la query generada
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException
     * @throws IOException Error I/O al escribir las querys 
     */
    public String sqlModificar(Object modificado) throws IllegalAccessException, 
            IllegalArgumentException, InvocationTargetException, IOException {
        String clave = "modificar-" + modificado.getClass().getSimpleName();
        String sql = ficheroQuerys.getProperty(clave);
        
        if (sql == null) {
            sql = crearModificar(modificado.getClass());
            ficheroQuerys.setProperty(clave, sql);
            ficheroQuerys.store(new FileOutputStream(FICHERO), "");
        }
        
        sql = parametrizarTodos(modificado, sql);
        sql = parametrizarClave(modificado, sql, false);
        
        return sql;
    }
    
    /**
     * Crea la query para obtener todos los registros de una tabla
     * @param clase DTO del que se quieren obtener todos los registros
     * @return la query generada
     * @throws IOException Error I/O al escribir las querys 
     */
    public String sqlObtener(Class clase) throws IOException {
        String clave = "obtener-" + clase.getSimpleName();
        String sql = ficheroQuerys.getProperty(clave);
        
        if (sql == null) {
            sql = crearObtener(clase);
            ficheroQuerys.setProperty(clave, sql);
            ficheroQuerys.store(new FileOutputStream(FICHERO), "");
        }
        
        return sql;
    }
    
    // --------- Generadores de SQL -----------
    
    /**
     * Genera la query de inserción
     * @param clase DTO del registro que se quiere inserar
     * @return query generada <i>sin parametrizar</i>
     */
    private String crearInsertar(Class clase) {
        String sql = "INSERT INTO " + obtenerNombreTabla(clase) + " (";
        int nCampos = 0;
        
        for (String prefijoGetter : new String[]{"get", "is"}) {
            for (Entry<String, Method> getter : Comunes.obtenerMetodos(clase, prefijoGetter).entrySet()) {
                if (!getter.getKey().equals("id")) {  // Comprueba que no es el id
                    sql += getter.getKey() + ",";
                    nCampos++;
                }
            }
        }
        sql = sql.substring(0, sql.length()-1);  // quita la coma sobrante (último carácter)
        
        sql += ") VALUES (";

        for (int i = 0; i < nCampos-1; i++) {
            sql += "?, ";
        }
        sql += "?)";
        
        return sql;
    }
    
    /**
     * Genera la query de borrado
     * @param clase DTO del registro que se quiere borrar
     * @return query generada <i>sin parametrizar</i>
     */
    private String crearBorrar(Class clase) {
        return "DELETE FROM " + obtenerNombreTabla(clase) +
                " WHERE id= ?";
    }
    
    /**
     * Genera la query de búsqueda por clave primaria
     * @param clase DTO del registro que se quiere buscar
     * @return query generada <i>sin parametrizar</i>
     */
    private String crearBuscar(Class clase) {
        return "SELECT * FROM " + obtenerNombreTabla(clase) +
                " WHERE id= ?";
    }
    
    /**
     * Genera la query de búsqueda por clave unique
     * @param clase DTO del registro que se quiere buscar
     * @return query generada <i>sin parametrizar</i>
     */
    private String crearBuscarUnique(Class clase) {
        return "SELECT * FROM " + obtenerNombreTabla(clase) +
                " WHERE " + Comunes.obtenerUnique(clase) + "= ?";
    }
    
    /**
     * Genera la query de modificación
     * @param clase DTO del registro que se quiere modificar
     * @return query generada <i>sin parametrizar</i>
     */
    private String crearModificar(Class clase) {
        String sql = "UPDATE " + obtenerNombreTabla(clase) + " SET ";
        
        for (String prefijoGetter : new String[]{"get", "is"}) {
            for (Entry<String, Method> getter : Comunes.obtenerMetodos(clase, prefijoGetter).entrySet()) {
                if (!getter.getKey().equals("id"))  // Comprueba que no es el id
                    sql += getter.getKey() + "= ?,";
            }
        }
        sql = sql.substring(0, sql.length()-1);  // quita la coma sobrante (último carácter)
        
        sql += " WHERE id=?";
        
        return sql;
    }
    
    /**
     * Genera la query para obtener todos los registros de una tabla
     * @param clase DTO de la tabla
     * @return query generada
     */
    private String crearObtener(Class clase) {
        return "SELECT * FROM " + obtenerNombreTabla(clase);
    }
    
    // --------- Parametrizadores -----------
    
    /**
     * Parametriza una query sustituyendo carácteres '?' con todos los 
     * campos de un DTO <b>excepto el id</b>
     * @param ob DTO del que extraer la información
     * @param sql query sin parametrizar
     * @return query parametrizada
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    private String parametrizarTodos(Object ob, String sql) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        StringBuilder sqlp = new StringBuilder(sql);
        int i = 0;
        
        for (String prefijoGetter : new String[]{"get", "is"}) {
            for (Entry<String, Method> getter : Comunes.obtenerMetodos(ob.getClass(), prefijoGetter).entrySet()) {
                if (!getter.getKey().equals("id")) {  // Comprueba que no es el id
                    boolean sustituido = false;
                    while (i < sqlp.length() && !sustituido) {
                        if (sqlp.charAt(i) == '?') {
                            // Sustituye un '?' por un valor
                            if (prefijoGetter.equals("is")) { // Si es un booleano hay que convertir true/false a 1/0
                                sqlp = sqlp.replace(i, i + 1, ((boolean) getter.getValue().invoke(ob) ? "1" : "0"));
                            } else {
                                sqlp = sqlp.replace(i, i + 1, "\"" + escaparComillas(getter.getValue().invoke(ob).toString()) + "\"");
                            }
                            sustituido = true;
                        }
                        i++;
                    }
                }
            }
        }
        
        return sqlp.toString();
    }
    
    /**
     * Parametriza una query sustituyendo el carácter '?' por la clave primaria
     * o unique de un DTO
     * @param ob DTO del que obtener la información
     * @param sql query sin parametrizar
     * @param unique true busca por clave unique, false busca por clave primaria
     * @return query parametrizada
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     * @throws InvocationTargetException 
     */
    private String parametrizarClave(Object ob, String sql, boolean unique) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        StringBuilder sqlp = new StringBuilder(sql);
        
        String campo = unique ? Comunes.obtenerUnique(ob.getClass()) : "id";
        Method getterClave = Comunes.obtenerMetodos(ob.getClass(), "get").get(campo);
        boolean encontrado = false;
        for (int i = 0; i < sqlp.length() && !encontrado; i++) {
            if (sqlp.charAt(i) == '?') {
                sqlp = sqlp.replace(i, i + 1, "\"" + escaparComillas(getterClave.invoke(ob).toString()) 
                        + "\"");
                encontrado = true;
            }
        }
        
        return sqlp.toString();
    }
    
    // --------- Métodos auxiliares -----------
    
    /**
     * Método para obtener el nombre de la tabla de un DTO en la base de datos 
     * a partir de la anotación MetaDTO
     * @param clase clase del DTO
     * @return nombre de la tabla
     */
    private String obtenerNombreTabla(Class clase) {
        return ((MetaDTO) clase.getAnnotation(MetaDTO.class)).nombreTabla();
    }
    
    /**
     * Método que escapa las comillas de un string
     * @param str string con comillas sin escapar
     * @return string con las comillas escapadas
     */
    private String escaparComillas(String str) {
        StringBuilder strb = new StringBuilder(str);
        
        for (int i = 0; i < strb.length(); i++) {
            if (strb.charAt(i) == '"') {
                strb = strb.insert(i, "\\");
                i++; // Evita que vuelva a encontrarse la misma comilla
            }
        }
        
        return strb.toString();
    }
}
