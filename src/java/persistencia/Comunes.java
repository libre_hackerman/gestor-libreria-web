/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Esta clase sirve como librería de métodos auxiliares utilizados por
 * distintas clases de persistencia
 * @author Esteban
 */
public class Comunes {

    /**
     * Obtiene getters, setters u otros métodos que comiencen por un prefijo
     * determinado
     * @param clase Clase de la que extraer lo métodos
     * @param prefijo inicio del nombre de los método. <i>Por ej, "get", "set"...</i>
     * @return Hashmap con los métodos. La clave es el nombre de lo que el método maneja <i>p. ej, titulo:setTitulo</i>
     */
    public static HashMap<String, Method> obtenerMetodos(Class clase, String prefijo) {
        HashMap<String, Method> mapaMetodos = new HashMap<>();

        for (Method method : clase.getDeclaredMethods()) {
            /*
             * Comprueba que el método empiece por el prefijo indicado y que el
             * caracter siguiente sea mayúscula. P. ej: isTapadura() es un
             * getter, pero isabel() no
             */
            if (method.getName().startsWith(prefijo) && Character.isUpperCase(method.getName().charAt(prefijo.length()))) {
                mapaMetodos.put(method.getName().substring(prefijo.length(), method.getName().length()).toLowerCase(), method);
            }
        }

        return mapaMetodos;
    }
    
    /**
     * Método para obtener el nombre del campo unique de un DTO en la base de datos 
     * a partir de la anotación MetaDTO
     * @param clase clase del DTO
     * @return nombre de la tabla
     */
    public static String obtenerUnique(Class clase) {
        return ((MetaDTO) clase.getAnnotation(MetaDTO.class)).campoUnique();
    }
}
