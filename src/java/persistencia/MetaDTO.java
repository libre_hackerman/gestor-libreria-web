/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2018-2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package persistencia;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/*
 * Tutorial sobre anotaciones: https://docs.oracle.com/javase/tutorial/java/annotations/index.html
 *
 * La anotación @Retention(RetentionPolicy.RUNTIME) hace que la
 * anotación [MetaDTO] sea retenida a lo largo de la ejecución del programa
 * https://docs.oracle.com/javase/8/docs/api/java/lang/annotation/Retention.html
 */

/**
 * Anotación asociada a los DTOs con información de utilidad para sus tablas
 * asociadas en la persistencia
 * @author Esteban
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface MetaDTO {
    String nombreTabla();
    String campoUnique() default "";
}
