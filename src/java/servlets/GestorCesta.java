/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import comun.Errores;
import comun.Mensajes;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import negocio.Cesta;
import negocio.Libro;
import persistencia.AccesoDAO;

@WebServlet("/gestorCesta")
public class GestorCesta extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Si se hace una peticion GET redirecciona al index
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Cesta cesta = (Cesta) request.getSession().getAttribute("cesta");
        boolean todoCorrecto = false;
        
        try {
            Libro libro = (Libro) new AccesoDAO().obtener(new Libro(Integer.parseInt(request.getParameter("id"))));   
            
            if (libro != null) {
                if ("sumar".equals(request.getParameter("operacion")))
                    cesta.sumarLibro(libro);
                else
                    cesta.restarLibro(libro);
                
                todoCorrecto = true;
            } else {
                request.setAttribute("message", Mensajes.msgNoEncontrado(request.getParameter("titulo")));
            }
        } catch (Exception e) {
            request.setAttribute("message", Errores.getError(e));
        }
        
        request.setAttribute("messageType", "bad");  // Si la operación ha tenido éxito, index.jsp ignorará este atributo 
        request.getRequestDispatcher(todoCorrecto ? "index.jsp" : "result.jsp").forward(request, response);
    }
    
}
