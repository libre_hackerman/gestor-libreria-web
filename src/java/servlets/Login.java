/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package servlets;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import comun.Errores;
import comun.Mensajes;
import java.io.IOException;
import javax.servlet.ServletException;
import persistencia.AccesoDAO;
import negocio.Usuario;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * Servlet implementation class UserLogin
 */
@WebServlet("/login")
public class Login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Si se hace una peticion GET redirecciona al index
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String nombre = request.getParameter("nombre");
        String passLimpia = request.getParameter("pass");
        boolean loginCorrecto = false;
        
        // Por defecto, considera que se producirá un mensaje negativo
        request.setAttribute("messageType", "bad");
        
        try {            
            Usuario usuario = (Usuario) new AccesoDAO().obtener(new Usuario(nombre));
            
            if (usuario != null && usuario.getPassHash().equals(DigestUtils.md5Hex(passLimpia))) {
                request.getSession().setAttribute("usuario", usuario);
                request.setAttribute("messageType", "good");
                loginCorrecto = true;
            } else {
                request.setAttribute("message", Mensajes.MSG_NO_LOGIN);
            }
            
        } catch (Exception e) {
            request.setAttribute("message", Errores.getError(e));
        }
        
        request.getRequestDispatcher(loginCorrecto ? "index.jsp" : "result.jsp").forward(request, response);
    }

}
