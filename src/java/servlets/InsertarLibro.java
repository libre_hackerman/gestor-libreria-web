/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import comun.Errores;
import comun.Mensajes;
import persistencia.AccesoDAO;
import negocio.Libro;


@WebServlet("/newBook")
public class InsertarLibro extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Si se hace una peticion GET redirecciona al index
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String titulo = request.getParameter("titulo");
        String autor = request.getParameter("autor");
        String tema = request.getParameter("tema");
        int precio = Integer.valueOf(request.getParameter("precio"));
        int paginas = Integer.valueOf(request.getParameter("paginas"));
        boolean cartone = "on".equals(request.getParameter("cartone"));
        boolean rustica = "on".equals(request.getParameter("rustica"));
        boolean tapaDura = "on".equals(request.getParameter("tapaDura"));
        boolean novedad = "novedad".equals(request.getParameter("novedad"));

        // Por defecto, considera que se producirá un mensaje negativo
        request.setAttribute("messageType", "bad");
        
        Libro libro = new Libro(-1, titulo, autor, tema, paginas, precio, cartone, rustica, tapaDura, novedad);
        try {  
            if ((new AccesoDAO()).insertar(libro, true) == AccesoDAO.COD_NO_CAMBIOS) {
                request.setAttribute("message", Mensajes.MSG_NO_CAMBIOS);
            } else { // El libro es insertado correctamente
                request.setAttribute("message", Mensajes.msgInsertado(titulo));
                request.setAttribute("messageType", "good");
            }

        } catch (Exception e) {
            request.setAttribute("message", Errores.getError(e));
        }

        request.getRequestDispatcher("result.jsp").forward(request, response);
    }

}
