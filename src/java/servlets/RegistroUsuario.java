/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */
package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import comun.Errores;
import comun.Mensajes;
import persistencia.AccesoDAO;
import negocio.Usuario;
import org.apache.commons.codec.digest.DigestUtils;

@WebServlet("/newUser")
public class RegistroUsuario extends HttpServlet {

    private static final int NIVEL_DEFECTO = 1;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Si se hace una peticion GET redireccionamos al index
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String nombre = request.getParameter("nombre");
        String passLimpia = request.getParameter("pass");
        String passLimpiaSeg = request.getParameter("passSeg");
        
        // Por defecto, considera que se producirá un mensaje negativo
        request.setAttribute("messageType", "bad");
        
        // Comprueba si los dos campos de contraseña coinciden
        if (!passLimpia.equals(passLimpiaSeg)) {
            request.setAttribute("message", Mensajes.MSG_PASS_DIFF);
            request.setAttribute("messageType", "warning");
        } else try {
            AccesoDAO dao = new AccesoDAO();
            
            // Solo inserta si no existe ya un usuario con el mismo nombre
            if (dao.obtener(new Usuario(nombre)) == null) {
                if (dao.insertar(new Usuario(-1, nombre, DigestUtils.md5Hex(passLimpia), NIVEL_DEFECTO), true) 
                        == AccesoDAO.COD_NO_CAMBIOS) {
                    request.setAttribute("message", Mensajes.MSG_NO_CAMBIOS);
                } else { // El usuario es insertado correctamente
                    request.setAttribute("message", Mensajes.msgInsertado(nombre));
                    request.setAttribute("messageType", "good");
                }
            } else {
                request.setAttribute("message", Mensajes.msgYaExiste(nombre));
                request.setAttribute("messageType", "warning");
            }
        } catch (Exception e) {
            request.setAttribute("message", Errores.getError(e));
        }

        request.getRequestDispatcher("result.jsp").forward(request, response);
    }

}
