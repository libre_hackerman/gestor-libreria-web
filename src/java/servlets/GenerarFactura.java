/*
 *        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *                    Version 2, December 2004 
 *
 * Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * Everyone is permitted to copy and distribute verbatim or modified 
 * copies of this license document, and changing it is allowed as long 
 * as the name is changed. 
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 */

package servlets;

import comun.Errores;
import comun.Mensajes;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import negocio.Cesta;
import negocio.Factura;
import negocio.LineasFactura;
import negocio.Usuario;
import persistencia.AccesoDAO;

@WebServlet("/joseFrancisco")
public class GenerarFactura extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Si se hace una peticion GET redirecciona al index
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        Cesta cesta = (Cesta) request.getSession().getAttribute("cesta");
        Usuario usuario = (Usuario) request.getSession().getAttribute("usuario");
        
         // Por defecto, considera que se producirá un mensaje negativo
        request.setAttribute("messageType", "bad");
        
        try {  
            AccesoDAO dao = new AccesoDAO();
            
            Factura factura = new Factura(-1, new Date().getTime(), usuario.getId());
            int idFactura = dao.insertar(factura, false);
            
            Object[] lineasFacturas = new LineasFactura[cesta.getLineas().size()];
            for (int i = 0; i < lineasFacturas.length; i++) {
                lineasFacturas[i] = new LineasFactura(-1, idFactura, cesta.getLineas().get(i).getCantidad(), 
                        cesta.getLineas().get(i).getLibro().getPrecio(), 
                        cesta.getLineas().get(i).getLibro().getTitulo());
            }
            
            if (dao.insertar(lineasFacturas) != null) {
                cesta.vaciar();
                request.setAttribute("message", Mensajes.msgInsertado("Factura " + idFactura));
                request.setAttribute("messageType", "good");
            } else {
                request.setAttribute("message", Mensajes.MSG_NO_CAMBIOS);
            }
        } catch (Exception e) {
            request.setAttribute("message", Errores.getError(e));
        }

        request.getRequestDispatcher("result.jsp").forward(request, response);
    }

}
