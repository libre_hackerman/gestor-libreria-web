<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <%-- Importamos la cabecera de la pagina --%>
    <c:import url="WEB-INF/header.jsp"/>
    <div class="col-xs-12 col-md-6 col-md-offset-3">
    <h3>Carrito de la compra</h3>

    <table  class="table table-striped">
        <thead>
            <tr class="info">
                <td>Titulo</td>
                <td>Cantidad</td>
                <td>Precio</td>
                <td>Total</td>
            </tr>
        </thead>
        <tbody>
    <c:forEach var="linea" items="${sessionScope.cesta.getLineas()}">
        <tr>
            <td><a href="viewBook.jsp?id=${linea.getLibro().getId()}&anterior=viewCart.jsp">
                ${linea.getLibro().getTitulo()}
            </a></td>
            <td>${linea.getCantidad()}</td>
            <td>${linea.getLibro().getPrecio()}€</td>
            <td>${linea.getTotal()}€</td>
        </tr>
    </c:forEach>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr class="success">
            <td></td>
            <td></td>
            <td>Total</td>
            <td>${sessionScope.cesta.getTotal()}€</td>
        </tr>
        </tbody>
    </table>
    </div>

    <div class="container text-center">
        <div class="row">
            <div class="col-xs-12">
                <c:set var="user" value="${sessionScope.get('usuario')}"/>
                <c:if test="${user != null}">
                    <!-- Easter egg -->
                    <form action="joseFrancisco" method="post">
                       <input class="btn btn-success" type="submit" value="Generar Factura">
                    </form>
                </c:if>
                <a type="button" class="btn btn-lg btn-default" href="index.jsp">Inicio</a>
            </div>
        </div>
    </div>

    <%-- Importamos el footer de la pagina --%>
<c:import url="WEB-INF/footer.jsp"></c:import>