<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="persistencia.AccesoDAO" %>

<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table class="table-striped">
    <tbody>
        <tr>
            <td>Nombre de usuario</td>
            <td><input class="form-control" type="text" name="nombre" required maxlength="${AccesoDAO.LIMITE_STRINGS}" /></td>

        </tr>
        <tr>
            <td>Contraseña</td>
            <td><input class="form-control" type="password" name="pass" required /></td>
        </tr>
        <tr>
            <td>Reescribir contraseña</td>
            <td><input class="form-control" type="password" name="passSeg" required /></td>
        </tr>	
    </tbody>
</table>