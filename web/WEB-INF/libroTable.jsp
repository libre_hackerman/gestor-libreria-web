<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="persistencia.AccesoDAO" %>

<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table class="table-striped">
    <tbody>
        <tr>
            <td>Título</td>
            <td>
                <c:choose>
                    <c:when test="${ !param.editable }"><c:out value="${ libro.getTitulo() }"></c:out></c:when>
                    <c:otherwise><input class="form-control" type="text" name="titulo"
                           maxlength="${AccesoDAO.LIMITE_STRINGS}"
                           value="${ libro.getTitulo() }" required>
                        <input type="hidden" value="${ libro.getId() }" name="id">
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
        <tr>
            <td>Autor</td>
            <td>
                <c:choose>
                    <c:when test="${ !param.editable }"><c:out value="${ libro.getAutor() }"></c:out></c:when>
                    <c:otherwise><input class="form-control" type="text" name="autor"
                           maxlength="${AccesoDAO.LIMITE_STRINGS}"
                           value="${ libro.getAutor() }" required></c:otherwise>
                </c:choose>
            </td>
        </tr>
        <tr>
            <td>Tema</td>
            <td>
                <c:choose>
                    <c:when test="${ !param.editable }"><c:out value="${ libro.getTema() }"></c:out></c:when>
                    <c:otherwise>

                        <select class=form-control name="tema">
                            <option <c:if test="${ libro.getTema().equals('Accion') }"> selected </c:if>>Accion</option>
                            <option <c:if test="${ libro.getTema().equals('Aventuras') }"> selected </c:if>>Aventuras</option>
                            <option <c:if test="${ libro.getTema().equals('Biografía') }"> selected </c:if>>Biografía</option>
                            <option <c:if test="${ libro.getTema().equals('Ciencia') }"> selected </c:if>>Ciencia</option>
                            <option <c:if test="${ libro.getTema().equals('Ciencia Ficción') }"> selected </c:if>>Ciencia Ficción</option>
                            <option <c:if test="${ libro.getTema().equals('Cine') }"> selected </c:if>>Cine</option>
                            <option <c:if test="${ libro.getTema().equals('DAM 2') }"> selected </c:if>>DAM 2</option>
                            <option <c:if test="${ libro.getTema().equals('Economía') }"> selected </c:if>>Economía</option>
                            <option <c:if test="${ libro.getTema().equals('Gastronomía') }"> selected </c:if>>Gastronomía</option>
                            <option <c:if test="${ libro.getTema().equals('Historia') }"> selected </c:if>>Historia</option>
                            <option <c:if test="${ libro.getTema().equals('Informática') }"> selected </c:if>>Informática</option>
                            <option <c:if test="${ libro.getTema().equals('Medicina') }"> selected </c:if>>Medicina</option>
                            <option <c:if test="${ libro.getTema().equals('Misterio') }"> selected </c:if>>Misterio</option>
                            <option <c:if test="${ libro.getTema().equals('Naturaleza') }"> selected </c:if>>Naturaleza</option>
                            <option <c:if test="${ libro.getTema().equals('Policíaco') }"> selected </c:if>>Policíaco</option>
                            <option <c:if test="${ libro.getTema().equals('Política') }"> selected </c:if>>Política</option>
                            <option <c:if test="${ libro.getTema().equals('Romántica') }"> selected </c:if>>Romántica</option>
                            <option <c:if test="${ libro.getTema().equals('Teatro') }"> selected </c:if>>Teatro</option>
                            <option <c:if test="${ libro.getTema().equals('Terror') }"> selected </c:if>>Terror</option>
                        </select>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
        <tr>
            <td>Numero de páginas</td>
            <td>
                <c:choose>
                    <c:when test="${ !param.editable }"><c:out value="${ libro.getPaginas()}"></c:out></c:when>
                    <c:otherwise><input class="form-control" type="number" name="paginas"
                           value="${ libro.getPaginas() }" required></c:otherwise>
                </c:choose>
            </td>
        </tr>
        
        <tr>
            <td>Precio</td>
            <td>
                <c:choose>
                    <c:when test="${ !param.editable }"><c:out value="${ libro.getPrecio() }"></c:out></c:when>
                    <c:otherwise><input class="form-control" type="number" name="precio"
                           value="${ libro.getPrecio() }" required></c:otherwise>
                </c:choose>
            </td>
        </tr>
        
        <tr>
            <td>Cartone</td>

            <td><input name="cartone" type="checkbox"
                       <c:if test="${ libro.isCartone() }">checked</c:if> <c:if test="${ !param.editable }">
                           disabled </c:if>></td>
            </tr>
            <tr>
                <td>Rustica</td>
                <td><input name="rustica" type="checkbox"
                    <c:if test="${ libro.isRustica()}">checked</c:if> <c:if test="${ !param.editable }">
                        disabled </c:if>></td>
            </tr>
            <tr>
                <td>Tapa dura</td>
                <td><input name="tapaDura" type="checkbox"
                    <c:if test="${ libro.isTapaDura()}">checked</c:if> <c:if test="${ !param.editable }">
                        disabled </c:if>></td>
            </tr>
            <tr>
                <td>Estado</td>
                <td>
                <c:choose>
                    <c:when test="${ !param.editable }"><c:out value="${ libro.isNovedad() ? 'Novedad' : 'Reedición'}"></c:out></c:when>
                    <c:otherwise>
                        <input type="radio" name="novedad" value="novedad"
                               <c:if test="${ libro.isNovedad() }">checked</c:if>>Novedad</input>
                        <input type="radio" name="novedad" value="reedicion"
                               <c:if test="${ !libro.isNovedad() }">checked</c:if>>Reedicion</input>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </tbody>
</table>
