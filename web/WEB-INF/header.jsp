<%@page import="negocio.Cesta"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="cesta" class="negocio.Cesta" scope="session"/>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Librería V6.8</title>
    <link rel="icon" 
      type="image/png" 
      href="media/pinkiepie2.png">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <nav class="navbar navbar-default" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.jsp">Librería V6.8</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <c:set var="user" value="${sessionScope.get('usuario')}"/>
            <c:choose>
                <c:when test="${user == null}">
                    <form class="navbar-form navbar-right" method="post" action="login">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Usuario" name="nombre">
                            <input type="password" class="form-control" placeholder="Password" name="pass">
                        </div>
                        <button type="submit" class="btn btn-default">Login</button>
                        <a href="registro.jsp" class="btn btn-default btn-success">Registrarse</a>
                    </form>
                </c:when>

                <c:otherwise>
                    <form class="navbar-form navbar-right" method="post" action="logout">
                        <button type="submit" class="btn btn-default">Logout - ${user.getNombre()}</button>
                    </form>
                </c:otherwise>
            </c:choose>
            
            <form class="navbar-form navbar-right" method="get" action="viewCart.jsp">
                <button type="submit" class="btn btn-default">Carrito <span class="badge">${cesta.numArticulos()}</span></button>
            </form>

            <c:if test="${user != null && user.getNivel()==5}">
                <form class="navbar-form navbar-right" method="get" action="viewUser.jsp">
                    <button type="submit" class="btn btn-warning">Panel Admin</button>
                </form>
            </c:if>
        </div>
    </nav>
<div class="container text-center">
    <h1>Librería V6.8</h1>
</div>

