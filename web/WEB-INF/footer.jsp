<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<footer class="footer">
    <div class="container text-center">
        <p class="text-muted">Version 6.4 | Miguel & Victor<sup> &copy;</sup> 2016-2017
         | Esteban López Rodríguez<sup> &copy;</sup> 2019</p>
    </div>
</footer>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.js"></script>
</body>
</html>