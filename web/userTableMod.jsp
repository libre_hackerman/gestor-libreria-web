<%@page import="comun.Errores"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:catch var="error">
<jsp:useBean id="dao" class="persistencia.AccesoDAO" scope="page"/>
<jsp:useBean id="usuarioMod" class="negocio.Usuario" scope="page"/>
${pageScope.usuarioMod.setNombre(param.nombre)}
<c:set var="usuarioMod" value="${pageScope.dao.obtener(usuarioMod)}" scope="request"/>

<table class="table-striped">
    <tbody>
        <tr>
            <td>Nombre de usuario</td>
            <td>
                <input type="hidden" name="nombreOrig" value="${ usuarioMod.getNombre() }"/>
                <input class="form-control" type="text" name="nombreMod" value="${ usuarioMod.getNombre() }"/>
                </td>
            </tr>
            <tr>
                <td>Password</td>
                <td>
                    <input class="form-control" type="password" name="passMod"/>
                </td>
            </tr>
        <td>Nivel</td>
        <td>
            <input type="range" name="nivelMod" id="nivelMod" value="${ usuarioMod.getNivel()}" min="1" max="5"
               oninput="nivelMod.value = nivelMod.value">
        <output name="levelOutputName" id="nivelMod" style="color: white" ></output>
    </td>

</tbody>
</table>
</c:catch>

<c:if test="${error != null}">
    <%
        request.setAttribute("message", Errores.getError((Exception) pageContext.getAttribute("error")));
        request.setAttribute("messageType", "bad");
        request.getRequestDispatcher("result.jsp").forward(request, response);
    %>
</c:if>