<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- Importamos la cabecera de la pagina --%>
<c:import url="WEB-INF/header.jsp"></c:import>

<%-- Zona para mensajes --%>
<c:if test="${requestScope.message != null }">
    <c:choose>
        <c:when test="${requestScope.messageType.equals('good')}">
            <div class="col-xs-12 col-md-10 col-md-offset-1 alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>¡Todo Correcto!</strong> <c:out value="${requestScope.message}"></c:out>
        </c:when>

        <c:when test="${requestScope.messageType.equals('warning')}">
            <div class="col-xs-12 col-md-10 col-md-offset-1 alert alert-dismissible alert-warning">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Advertencia:</strong> <c:out value="${requestScope.message}"></c:out>
        </c:when>

        <c:otherwise>
            <div class="col-xs-12 col-md-10 col-md-offset-1 alert alert-dismissible alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>¡Error!</strong> <c:out value="${requestScope.message}"></c:out>
        </c:otherwise>
    </c:choose>
    </div>
</c:if>
<%-- /Zona de mensajes --%>

<div class="container text-center">
    <div class="row">
        <div class="col-xs-12">
            <a type="button" class="btn btn-lg btn-default" href="index.jsp">Inicio</a>
        </div>
    </div>
</div>

<%-- Importamos el footer de la pagina --%>
<c:import url="WEB-INF/footer.jsp"></c:import>