<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="comun.Errores" %>
<%@ page import="comun.Mensajes" %>
<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:catch var="error">

    <jsp:useBean id="dao" class="persistencia.AccesoDAO" scope="page"/>
    <jsp:useBean id="libro" class="negocio.Libro" scope="page"/>

    <%-- Importamos la cabecera de la pagina --%>
    <c:import url="WEB-INF/header.jsp"/>

    <%-- Libro: Hacemos que tenga scope="request" para que sea accesible en libroTable.jsp --%>
    ${pageScope.libro.setId(param.id)}
    <c:set var="libro" value="${pageScope.dao.obtener(libro)}" scope="request"/>

    <c:choose>
        <c:when test="${ requestScope.libro != null }">
            <div class="container">
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                    <h2 class="text-center"><c:out value="${ libro.getTitulo() }"/></h2>

                    <h3>Datos:</h3>
                        <%-- Importamos la tabla del libro --%>
                    <c:import url="WEB-INF/libroTable.jsp">
                        <c:param name="editable" value="false"/>
                    </c:import>

                    <c:set var="user" value="${sessionScope.get('usuario')}"/>
                    <c:if test = "${user != null && user.getNivel() == 5}">
                        <a type="button" class="btn btn-primary pull-right" href="editBook.jsp?id=${ libro.getId() }">Editar</a>
                        <form action="deleteBook" method="post">
                            <input type="hidden" value="${ libro.getId() }" name="id">
                            <input type="hidden" value="${ libro.getTitulo() }" name="titulo">
                            <input class="btn btn-danger pull-right" type="submit" value="Borrar"/>
                        </form>
                    </c:if>
                        
                    <form action="gestorCesta" method="post">
                        <input type="hidden" value="${ libro.getId() }" name="id">
                        <input type="hidden" value="sumar" name="operacion">
                        <input class="btn btn-success pull-right" type="submit" value="Añadir a la cesta"/>
                    </form>
                    
                    <a type="button" class="btn btn-default" href="${param.anterior}">Volver</a>
                </div>
            </div>
        </c:when>

        <c:otherwise>
            <%
                request.setAttribute("message", Mensajes.msgNoEncontrado("Libro [" + libro.getTitulo() + "]"));
                request.setAttribute("messageType", "bad");
                request.getRequestDispatcher("result.jsp").forward(request, response);
            %>
        </c:otherwise>
    </c:choose>


    <%-- Importamos el footer de la pagina --%>
    <c:import url="WEB-INF/footer.jsp"></c:import>

</c:catch>

<c:if test="${error != null}">
    <%
        request.setAttribute("message", Errores.getError((Exception) pageContext.getAttribute("error")));
        request.setAttribute("messageType", "bad");
        request.getRequestDispatcher("result.jsp").forward(request, response);
    %>
</c:if>