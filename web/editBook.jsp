<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="comun.Errores" %>
<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:catch var="error">

    <jsp:useBean id="dao" class="persistencia.AccesoDAO" scope="page"></jsp:useBean>
    <jsp:useBean id="libro" class="negocio.Libro" scope="page"/>
    <%-- Importamos la cabecera de la pagina --%>
    <c:import url="WEB-INF/header.jsp"></c:import>

    <%-- Libro: Hacemos que tenga scope="request" para que sea accesible en libroTable.jsp --%>
    ${pageScope.libro.setId(param.id)}
    <c:set var="libro" value="${pageScope.dao.obtener(libro)}" scope="request"></c:set>

    <c:choose>
        <c:when test="${ requestScope.libro != null }">
            <div class="container">		
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                    <h2 class="text-center"><c:out value="${ libro.getTitulo() }"></c:out></h2>
                    <h3>Datos:</h3>
                    <form action="editBook" method="post">
                        <%-- Importamos la tabla del libro --%>
                        <c:import url="WEB-INF/libroTable.jsp">
                            <c:param name="editable" value="true"></c:param>
                        </c:import>

                        <input class="btn btn-success pull-right" type="submit" value="Guardar"/>
                    </form>		
                    <a type="button" class="btn btn-default" href="index.jsp">Volver</a>
                </div>			
            </div>	
        </c:when>

        <c:otherwise>
            <%-- Si no se encuentra el libro redireccionamos al index --%>
            <c:redirect url="index.jsp"></c:redirect>
        </c:otherwise>
    </c:choose>



    <%-- Importamos el footer de la pagina --%>
    <c:import url="WEB-INF/footer.jsp"></c:import>

</c:catch>

<c:if test="${error != null}">
    <%
        request.setAttribute("message", Errores.getError((Exception) pageContext.getAttribute("error")));
        request.setAttribute("messageType", "bad");
        request.getRequestDispatcher("result.jsp").forward(request, response);
    %>
</c:if>