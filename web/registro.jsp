<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<%-- Importamos la cabecera de la pagina --%>
<c:import url="WEB-INF/header.jsp"></c:import>

    <div class="container">
        <h2>Registro de usuarios:</h2>
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <form action="newUser" method="post">
                <c:import url="WEB-INF/userTable.jsp"></c:import>
                <input class="btn btn-success pull-right" type="submit" value="Registrarse"/>
            </form>
        </div>
    </div>

<%-- Importamos el footer de la pagina --%>
<c:import url="WEB-INF/footer.jsp"></c:import>