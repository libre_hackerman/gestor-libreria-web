<%@ page import="comun.Errores" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:catch var="error">
    <%-- Importamos la cabecera de la pagina --%>
    <c:import url="WEB-INF/header.jsp"/>
    <div class="col-xs-12 col-md-6 col-md-offset-3">

        <h2>Lista de usuarios:</h2>
        <jsp:useBean id="dao" class="persistencia.AccesoDAO" scope="page"/>
        <c:set var="usuarios" value="${pageScope.dao.obtener('negocio.Usuario')}"/>
        <div class="col-xs-12">
            <c:if test="${usuarios.isEmpty() }">
                <p>No hay usuarios que mostrar</p>
            </c:if>
            <ul>
                <c:forEach var="usuario" items="${usuarios}">
                    <li><a href="editUser.jsp?nombre=${usuario.getNombre()}">${usuario.getNombre()}</a></li>
                </c:forEach>
            </ul>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <a type="button" class="btn btn-lg btn-default" href="index.jsp">Inicio</a>
            </div>
        </div>
    </div>
    <%-- Importamos el footer de la pagina --%>
    <c:import url="WEB-INF/footer.jsp"></c:import>
</c:catch>
    <c:if test="${error != null}">
        <%
            request.setAttribute("message", Errores.getError((Exception) pageContext.getAttribute("error")));
            request.setAttribute("messageType", "bad");
            request.getRequestDispatcher("result.jsp").forward(request, response);
        %>
    </c:if>