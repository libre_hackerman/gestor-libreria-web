<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- Importamos la cabecera de la pagina --%>
<c:import url="WEB-INF/header.jsp"></c:import>

    <div class="container">		
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <h3>Datos:</h3>
            <form action="newBook" method="post">
            <%-- Importamos la tabla del libro --%>
            <c:import url="WEB-INF/libroTable.jsp">
                <c:param name="editable" value="true"></c:param>
            </c:import>				
            <input class="btn btn-danger pull-right" type="submit" value="Guardar"/>
        </form>
        <a type="button" class="btn btn-default" href="index.jsp">Volver</a>	
    </div>			
</div>	




<%-- Importamos el footer de la pagina --%>
<c:import url="WEB-INF/footer.jsp"></c:import>