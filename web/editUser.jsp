<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="WEB-INF/header.jsp"/>

<form action="usuarioMod" method="post">
    <c:import url="userTableMod.jsp"/>
    <input class="btn btn-success pull-right" type="submit" value="Modificar usuario"/>
</form>

<a type="button" class="btn btn-lg btn-default" href="index.jsp">Inicio</a>
<%-- Importamos el footer de la pagina --%>
<c:import url="WEB-INF/footer.jsp"/>

