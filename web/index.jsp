<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="comun.Errores" %>
<%-- Importamos el core de JSTL --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%-- Importamos la cabecera de la pagina --%>
<c:import url="WEB-INF/header.jsp"/>

<div class="container">
    <h2>Lista de libros:</h2>
    <c:catch var="error">
        <jsp:useBean id="dao" class="persistencia.AccesoDAO" scope="page"/>
        <c:set var="libros" value="${pageScope.dao.obtener('negocio.Libro')}"/>
        <div class="col-xs-12">
            <c:if test="${libros.isEmpty() }">
                <p>No hay Libros que mostrar</p>
            </c:if>
            <ul>
                <c:forEach var="libro" items="${libros}">
                    <li><a href="viewBook.jsp?id=${libro.getId()}&anterior=index.jsp">${libro.getTitulo()}</a></li>
                    </c:forEach>
            </ul>

            <c:set var="user" value="${sessionScope.get('usuario')}"/>
            <c:if test = "${user != null && user.getNivel() == 5}">
                <a type="button" class="btn btn-primary" href="newBook.jsp">Añadir libro</a>
            </c:if>
        </div>
    </c:catch>
    <c:if test="${error != null}">
        <%
            request.setAttribute("message", Errores.getError((Exception) pageContext.getAttribute("error")));
            request.setAttribute("messageType", "bad");
            request.getRequestDispatcher("result.jsp").forward(request, response);
        %>
    </c:if>
</div>

<%-- Importamos el footer de la pagina --%>
<c:import url="WEB-INF/footer.jsp"/>





